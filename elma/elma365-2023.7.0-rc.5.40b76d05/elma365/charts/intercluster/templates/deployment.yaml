{{- if and (eq .Values.global.solution "saas") (eq .Values.global.edition "standard") }}
{{- $releaseName := .Release.Name }}
{{- $name := include "intercluster.name" . }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ template "intercluster.name" . }}
  labels:
    {{- include "intercluster.labels" . | nindent 4 }}
  annotations:
    {{- with .Values.global.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
    {{- with .Values.annotations }}
    {{- toYaml . | nindent 4 }}
    {{- end }}
spec:
  {{- if not .Values.global.autoscaling.enabled }}
  replicas: {{ if or (kindIs "float64" .Values.replicaCount) (kindIs "int64" .Values.replicaCount) }}{{ .Values.replicaCount }}{{ else }}{{ .Values.global.replicaCount }}{{ end }}
  {{ end }}
  selector:
    matchLabels:
      {{- include "intercluster.selectorLabels" . | nindent 6 }}
  strategy: {{- toYaml .Values.global.updateStrategy | nindent 4 }}
  template:
    metadata:
      annotations:
        {{- with .Values.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
        {{- with .Values.global.podAnnotations }}
        {{- toYaml . | nindent 8 }}
        {{- end }}
      labels:
        {{- include "intercluster.selectorLabels" . | nindent 8 }}
    spec:
      imagePullSecrets:
        {{- range .Values.global.image.pullSecret }}
        - name: {{ . }}
        {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.global.image.repository }}/elma365/intercluster/service:{{ .Values.images.service }}"
          imagePullPolicy: {{ .Values.global.image.pullPolicy }}
          ports:
            - name: grpc
              containerPort: {{ .Values.global.grpc_port }}
            - name: http
              containerPort: {{ .Values.global.http_port }}
          env:
            - name: HOST_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.hostIP
            - name: NAMESPACE
              value: {{ .Release.Namespace | quote }}
            - name: ELMA365_SKIP_SSL_VERIFY
              value: {{ .Values.global.skipSslVerify | quote }}
            - name: APPEND_VAHTER_SERVICE_NAME_TO_COLLECTION_PREFIX
              value: {{ .Values.appconfig.appendVahterServiceNameToCollectionPrefix | quote }}
            - name: VAHTER_MONGO_URL
              valueFrom:
                secretKeyRef:
                  name: elma365-db-connections
                  key: VAHTER_MONGO_URL
            - name: ELMA365_INTERCLUSTER_ALLOW_EXPORT
              value: {{ .Values.appconfig.allowExport | quote }}
            - name: ELMA365_INTERCLUSTER_ALLOW_IMPORT
              value: {{ .Values.appconfig.allowImport | quote }}
          envFrom:
            - configMapRef:
                name: elma365-env-config
                optional: true
            - secretRef:
                name: elma365-db-connections
                optional: true
        {{- if .Values.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.livenessProbe | nindent 12 }}
        {{- else if .Values.global.livenessProbe }}
          livenessProbe:
          {{- toYaml .Values.global.livenessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.readinessProbe | nindent 12 }}
        {{- else if .Values.global.readinessProbe }}
          readinessProbe:
          {{- toYaml .Values.global.readinessProbe | nindent 12 }}
        {{- end }}
        {{- if .Values.startupProbe }}
          startupProbe:
          {{- toYaml .Values.startupProbe | nindent 12 }}
        {{- else if .Values.global.startupProbe }}
          startupProbe:
          {{- toYaml .Values.global.startupProbe | nindent 12 }}
        {{- end }}
        {{- if or .Values.global.autoscaling.enabled .Values.autoscaling.enabled }}
        {{- if or ( eq .Values.global.edition "enterprise" ) ( not ( eq .Values.global.solution "onPremise" ) ) }}
          resources:
          {{- if ne (len .Values.resources) 0 -}}
          {{- toYaml .Values.resources | nindent 12 -}}
          {{- else -}}
          {{- toYaml .Values.global.resources | nindent 12 -}}
          {{- end }}
        {{- end }}
        {{- end }}
    {{- with .Values.global.nodeSelector }}
      nodeSelector:
{{ toYaml . | indent 8 }}
    {{- end }}
    {{- with .Values.global.tolerations }}
      tolerations:
{{ toYaml . | indent 8 }}
    {{- end }}
      affinity:
    {{- with .Values.global.affinity }}
{{ toYaml . | indent 8 }}
    {{- else }}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - {{ template "intercluster.name" . }}
                    - key: release
                      operator: In
                      values:
                        - "{{ .Release.Name }}"
                topologyKey: kubernetes.io/hostname
              weight: 10
    {{- end }}
{{- end }}
